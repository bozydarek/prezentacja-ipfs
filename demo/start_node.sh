#!/bin/bash

mkdir /tmp/ipfs-docker-staging
mkdir /tmp/ipfs-docker-data

docker run -d --name ipfs-node \
  -v /tmp/ipfs-docker-staging:/export \
  -v /tmp/ipfs-docker-data:/data/ipfs \
  -p 4001:4001 \
  -p 127.0.0.1:8080:8080 \
  -p 127.0.0.1:5001:5001 \
  ipfs/go-ipfs:latest

docker ps
docker logs -f ipfs-node
