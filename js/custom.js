const text = ['Permanent', 'Merkle', 'Distributed'];

textSequence(0);
function textSequence(i) {

    if (text.length > i) {
        setTimeout(function() {
            document.getElementById("sequence").innerHTML = text[i];
            textSequence(++i);
        }, 2000); // 1 second (in milliseconds)

    } else if (text.length === i) { // Loop
        textSequence(0);
    }

}